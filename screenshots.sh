#~/bin/bash
# Script to make freesurfer screenshots from comma separated file.
# Assumes that the file has a header and is comma separated,
# first column is ignored subsequent columns include
# vertices for the left then right hemisphere of the
# fsaverage5 freesurfer template. NAs are relaced by zeros.
# maximum threshold is taken as the 75 quantile of the nonzero
# values, minimum is specified below. First input is file name.
# Second input is output directory.
#
# Written by Simon Vandekar 20161007 on a MAC OS.


file=$1
# directory is created
outdir=$2
mkdir -p $outdir 2>/dev/null
# minimum absolute value to display. If min=0 then medial wall will show red
min=0.001
pngs=${outdir}/pngs
ascs=${outdir}/ascs
mghs=${outdir}/mghs
mkdir $pngs 2> /dev/null
mkdir $ascs 2> /dev/null
mkdir $mghs 2> /dev/null


if [ -z ${FREESURFER_HOME} ]; then
	
	stop
fi

# constants
rh=$FREESURFER_HOME/subjects/fsaverage5/surf/rh.inflated
lh=$FREESURFER_HOME/subjects/fsaverage5/surf/lh.inflated
lhs=$FREESURFER_HOME/subjects/fsaverage5/surf/lh.sphere
rhs=$FREESURFER_HOME/subjects/fsaverage5/surf/rh.sphere

# convert template thickness to ascii format to be sure of file format
mris_convert -c $FREESURFER_HOME/subjects/fsaverage5/surf/lh.thickness $lhs $FREESURFER_HOME/subjects/fsaverage5/surf/lh.thickness.asc
# assumes same number of vertices for both hemispheres
nl=$(cat $FREESURFER_HOME/subjects/fsaverage5/surf/lh.thickness.asc | wc -l)
cut -d" " -f 1-4 $FREESURFER_HOME/subjects/fsaverage5/surf/lh.thickness.asc > $outdir/lh.std
cut -d" " -f 1-4 $FREESURFER_HOME/subjects/fsaverage5/surf/rh.thickness.asc > $outdir/rh.std


# get data for left hemisphere
head -n $(($nl + 1)) $file | tail -n +2 > $outdir/lh.temp
tail -n +$(($nl + 2)) $file > $outdir/rh.temp
sed -i '' -e "s+NA+0+g" $outdir/lh.temp
sed -i '' -e "s+NA+0+g" $outdir/rh.temp
ncol=$(head -n1 $outdir/lh.temp | grep -o "," | wc -l)
cols=$(seq 2 $(($ncol + 1)))

for i in ${cols[@]}; do
	# column name. Removes all special characters
	name=$(head -n1 $file | cut -d, -f $i | tr -dc '[:alnum:]\n\r')

	# LEFT HEMISPHERE
	paste -d" " $outdir/lh.std <(cut -d, -f $i $outdir/lh.temp) > "$ascs/lh.${name}.asc"
	cut -d" " -f5 $ascs/lh.$name.asc | sed "s+-++g" | sort -n | sed -e "s///" > temp
	q=$(cat temp | grep -n '^0$' | tail -n 1 | cut -d":" -f1)
	# 1/4 of vertices are above the max threshold
	maxind=$(echo "scale=0; $nl*3/4+$q/4" | bc)
	max=$(cat temp | sed  -n "${maxind}p")
	# rounds 3 decimal places
	max=$(echo "scale=3; $max/1" | bc)
	#echo $min,$max

	lims=$min,$max
	limsout=$(echo $lims | tr "," "-")
	im=$mghs/lh.$name.mgh
	mris_convert -c $ascs/lh.$name.asc $lhs $im
	imname=$(basename $im)
	#echo $imname | sed "s+.mgh+${limsout}.png+g"
	imname1=$(echo $imname | sed "s+.mgh+_medial_${limsout}.png+g")
	imname2=$(echo $imname | sed "s+.mgh+_lateral_${limsout}.png+g")
	freeview -viewport 3d -zoom 1.8 -ss $pngs/$imname2 -f $lh:overlay=$im:overlay_threshold=$lims:edgethickness=0
	freeview -viewport 3d -zoom 1.8 -cam Elevation 200 -ss $pngs/$imname1 -f $lh:overlay=$im:overlay_threshold=$lims:edgethickness=0

	paste -d" " $outdir/rh.std <(cut -d, -f $i $outdir/rh.temp) > "$ascs/rh.${name}.asc"
	im=$mghs/rh.$name.mgh
	mris_convert -c $ascs/rh.$name.asc $rhs $im
	imname=$(basename $im)
	#echo $imname | sed "s+.mgh+${limsout}.png+g"
	imname1=$(echo $imname | sed "s+.mgh+_medial_${limsout}.png+g")
	imname2=$(echo $imname | sed "s+.mgh+_lateral_${limsout}.png+g")
	freeview -viewport 3d -zoom 1.8 -cam Elevation -20 -ss $pngs/$imname1 -f $rh:overlay=$im:overlay_threshold=$lims:edgethickness=0
	freeview -viewport 3d -zoom 1.8 -cam Elevation 180 -ss $pngs/$imname2 -f $rh:overlay=$im:overlay_threshold=$lims:edgethickness=0
done
